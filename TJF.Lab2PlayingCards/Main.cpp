#include <conio.h>
#include <iostream>
#include <string>

using namespace std;


enum Suit 
{
	//Ordered so that standard convention of using alphabetical order can be applied 
	// Clubs < Diamonds < Hearts < Spades in case of rank tie - ace of spades beats ace of clubs)
	CLUBS,
	DIAMONDS,
	HEARTS,
	SPADES

};

enum Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

struct Card 
{
	Suit Suit;
	Rank Rank;
};


int main()
{

	return 0;
}